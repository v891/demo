package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity(name = "person")
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String email;
}
