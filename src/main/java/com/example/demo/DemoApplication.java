package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Person build = Person.builder().id(UUID.randomUUID().toString())
                .firstName("Jon")
                .lastName("Doe").email("jon_due@yahoo.co.in").build();

        personRepository.save(build);

        List<Person> all = personRepository.findAll();

        all.stream().forEach(p -> System.out.println(p.toString()));
    }


}

