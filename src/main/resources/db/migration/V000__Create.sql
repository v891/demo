CREATE TABLE person
                (
                   id varchar(36) PRIMARY KEY NOT NULL,
                   first_name varchar(10) NOT NULL,
                   last_name varchar(10) NOT NULL,
                   email varchar (32) NOT NULL
                );